#!/usr/bin/env node
import debugFn from 'debug'
import yargs from 'yargs/yargs'
import { hideBin } from 'yargs/helpers'
import createQunit from './index.mjs'
// argv
const argv = yargs(hideBin(process.argv)).argv
debugFn('create-qunit:cli')('create-qunit arguments:', argv)
// run
createQunit(argv)
