// create-qunit@2.X upgrade to ESM
// npx create-qunit@latest or npm init qunit
// or call this cli interface it will run a setup
import { join, resolve } from 'node:path'
import fsx from 'fs-extra'
import stringifyObject from 'stringify-object'
import { chainPromises, baseConfig, getDirname } from './src/helpers.mjs'
// setup
const __dirname = getDirname(import.meta.url)
const baseDir = process.cwd()
const srcDir = join(__dirname, 'src')
// this is our package.json here
const localPkg = fsx.readJsonSync(join(__dirname, 'package.json'))
// replace the follow in the result html file
const qunitVer = localPkg.devDependencies.qunit.substring(1) // get rip of the first char
// add this two to their existing devDependencies
const packageToAdd = ['server-io-core', 'qunit']
const close = (num = 1) => process.exit(num)
// main
export default function createQunit (argv) {
  // first thing we need to to is actually read the target project package.json
  // if it doesn't exist then exit!
  const packageJsonFile = join(baseDir, 'package.json')
  // add check if file exist first
  if (!fsx.existsSync(packageJsonFile)) {
    console.error('ERROR: you got to init project before you can use this', packageJsonFile)
    return close()
  }
  const pkg = fsx.readJsonSync(packageJsonFile)
  if (!pkg) {
    console.error('ERROR: fail to read your package.json file')
    return close()
  }
  const projectName = pkg.name
  let userBaseDir
  if (argv.baseDir && fsx.existsSync(resolve(argv.baseDir))) {
    userBaseDir = resolve(argv.baseDir)
  } else { // v1.1.0
    const testsDir = join(baseDir, 'tests')
    // we just create it if it doesn't exist
    try {
      fsx.ensureDirSync(testsDir)
      userBaseDir = testsDir
    } catch (e) {
      console.error(`Fail to create directory ${testsDir}`, e)
      return close()
    }
  }
  if (userBaseDir) {
    console.info('Your qunit test directory will be', userBaseDir)
  }
  // we only interested with create-qunit ./dist,./other_folder
  let addedDirs = argv._
  if (addedDirs.length) {
    addedDirs = addedDirs
      .map(dir => resolve(dir))
      .filter(dir => fsx.existsSync(dir))
    console.info('additional directory will add to webroot', addedDirs)
  }
  // now add with ours
  const qunitDir = userBaseDir ? join(userBaseDir, 'qunit') : join(baseDir, 'qunit')
  // this need transform
  const html = join('webroot', 'index.html')
  const files = [
    join('files', 'hello-world.qunit.js'),
    join('webroot', 'qunit-helper.js')
    // join('run-qunit-setup.mjs')
  ]
  const jobs = files
    .map(file => fsx.copy(
      join(srcDir, 'fixtures', file),
      join(qunitDir, file)
    ))
  jobs.push(
    fsx.readFile(join(srcDir, 'fixtures', html)).then(data => {
      return fsx.outputFile(
        join(qunitDir, html),
        data.toString()
          .replace(/\{\{VERSION\}\}/g, qunitVer)
          .replace(/\{\{NAME\}\}/g, projectName)
      )
    })
  )
  // run jobs
  return chainPromises(jobs)
    .catch(error => {
      console.error('Sorry we encounter an error and have to quit', error)
      close()
    })
    .then(() => {
      console.info('Files copy successfully, now creating config file')
      // continue with the generate config file
      baseConfig.baseDir = userBaseDir || baseDir
      // need to fix the webroot directory here
      baseConfig.webroot = baseConfig.webroot.map(dir => join(baseConfig.baseDir, dir))
      // then add the dev added directory
      baseConfig.webroot = Array.isArray(addedDirs) && addedDirs.length
        ? baseConfig.webroot.concat(addedDirs)
        : baseConfig.webroot
      // generate the file
      // @TODO we need to use dynamic base path instead of hard code full path 
      // otherwise this won't be portable
      const content = `// start up for qunit
import { getConfigForQunit } from 'server-io-core/helper.mjs'
import { serverIoCore } from 'server-io-core/index.mjs'
// combine method
async function runQunitSetup (userConfig) {
  return getConfigForQunit(userConfig)
    .then(serverIoCore)
    .catch(err => {
      console.error(err)
    })
}

const config = ${stringifyObject(baseConfig, { indent: '  ' })}

runQunitSetup(config)
`
      fsx.outputFile(join(baseConfig.baseDir, 'qunit', 'run-qunit.mjs'), content, err => {
        if (err) {
          console.error('We couldn\'t generate the run file. Exit now', err)
          return close()
        }
        const qunitScript = 'qunit/run-qunit.mjs'
        const scriptPath = userBaseDir ? join(userBaseDir, qunitScript) : './' + qunitScript
        // now add stuff to the package.json
        pkg.scripts.qunit = `node ${scriptPath.replace(baseDir + '/', './')}`
        // v1.1.0
        // check to see if there is an AVA setup
        if (pkg.ava) {
          pkg.ava.files = pkg.ava.files || []
          const ignorePath = userBaseDir ? join(userBaseDir, 'qunit') : './qunit'
          pkg.ava.files.push(`!${ignorePath.replace(baseDir + '/', '')}/*.*`)
        }
        // add devDependencies
        if (!pkg.devDependencies) {
          pkg.devDependencies = {}
        }
        pkg.devDependencies = Object.assign(
          pkg.devDependencies,
          packageToAdd.map(pkgName => {
            return { [pkgName]: localPkg.devDependencies[pkgName] }
          })
            .reduce((a, b) => Object.assign(a, b), {})
        )
        // output the package.json again
        fsx.writeJson(packageJsonFile, pkg, { spaces: 2 }, function (err) {
          if (err) {
            console.error('Sorry we couldn\'t write the config to package.json', err)
            return
          }
          console.info('Your package.json has been updated')
          // add dependencies
          if (process.env.NODE_ENV !== 'test' && !argv.skipInstall) {
            console.info('Please run `npm install`')
          } else {
            if (argv.skipInstall) {
              console.info('Please run `npm install`')
            } else {
              console.info('This is all done, exit now')
            }
          }
        })
      })
    })
}
