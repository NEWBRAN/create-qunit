# create-qunit

> 在浏览器和 node 中运行快速简便的 Qunit 测试套件

[English README](https://gitee.com/NEWBRAN/create-qunit/blob/master/README.en.md)

为什么？因为我认为大部分的浏览器测试套件是垃圾!

我只是想快速测试一个想法,设置浏览器测试套件的时间是荒谬的!!!
最糟糕的部分是 - 我经常发现自己调试[f**king](https://jasmine.github.io/),[一堆屎](https://jestjs.io/)工具,然后实际处理我的代码...


[qunit](https://qunitjs.com) 存在很长一段时间, 简单, 但猜猜什么 - 它无毛病!

## 安装

```sh
$ npx create-qunit@latest 
```

或

```sh
$ npm init qunit
```

## 配置

安装后,您应该在项目根上找到一个 `qunit` 文件夹。在里面你应该找到一个
`run-qunit.js` 文件。在这里您可以编辑配置选项。

此外,它会将一个脚本插入到您的 `package.json`, 你可以只运行 `npm run qunit` 看看会发生什么。

## 选项

### 设置基本目录 --baseDir

您可以通过传递 `--baseDir` 来设置放置 `qunit` 文件夹的位置

```sh
$ npx create-qunit --baseDir ./tests
```

然后,`qunit` 文件夹和所有关联文件将位于 `./tests/qunit`


### 额外 根目录 (webroot)

您可以在设置期间(并且应该)添加额外根目录。

```sh
$ npx create-qunit ./dist
```

它们将添加到配置选项的 `webroot` 中,当开发人员服务器启动时,它们就会得到服务。
然后,您只需要编辑 `index.html` 添加您的代码。

例如,您正在处理一个称为 `awesome-script` 的项目,您的分发代码位于 `dist/awesome-script.js` 中。

然后只需将其添加到`index.html`

```html
<script src="/awesome-script.js"></script>
```

原因是 webroot 指向 `dist`,然后为文件所在的浏览器提供内容。

### 跳过安装 --skipInstall

~~在安装结束时,它将运行 `npm install` 以添加额外的依赖项。如果您不需要,可以添加 `--skipInstall`~~



## server-io-core 附加配置

您可以使用 [server-io-core](https://gitlab.com/newbranltd/server-io-core) 设置更多选项。特别是
当您需要服务器端代码来测试您的 javascript 代码时。您可以使用代理选项。

---

ISC

[Joel Chu](https://joelchu.com)

[NEWBRAN LTD](https://newbran.co.uk) (c) 2022

[TO1SOURCE](https://to1source.cn)
