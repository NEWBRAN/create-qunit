# create-qunit

> A quick and simple way to setup browser test with Qunit

[中文 README](https://gitee.com/NEWBRAN/create-qunit/blob/master/README.md)

Why? Because I think most of the browser testing suite out there sucks - BIG TIME!

I just wanna to quickly test an idea, and the amount of time to setup a browser testing suite is ridiculous!!!
And the worst part is - I often found myself debugging the [f**king](https://jasmine.github.io/) piece of [sh*t](https://jestjs.io/) tools then actually working on my code ...

[Qunit](https://qunitjs.com) been around for a long time, it's simple but guess what - it works!

## Installation

```sh
$ npx create-qunit@latest
```

or

```sh
$ npm init qunit
```

## Configuration

After the installation you should find a `qunit` folder on your project root. And inside you should find a
`run-qunit.mjs` file. And thats where you can edit the configuration options.

Also it will insert a script into your `package.json`, you can just run `npm run qunit` and see what happen.

## options

### set base directory

You can set where to put the `qunit` folder by passing the `--baseDir`

```sh
$ npx create-qunit --baseDir ./tests
```

Then the `qunit` folder and all associate files will be in `./tests/qunit`

### Additional webroots

You can (and should) pass the additional directory during setup.

```sh
$ npx create-qunit ./dist
```

And they will add to the `webroot` of the configuration options, they will get serve up when the developer server started.
Then you just have to edit the `index.html` to include your library code.

For example, you are working on a project call `awesome-script` and your distribute code is in `dist/awesome-script.js`

Then just add it to `index.html`

```html
<script src="/awesome-script.js"></script>
```

The reason is the webroot is pointing to `dist` then serve up the content inside, for the browser that's where the file will be.

### skipInstall

~~By the end of the installation it will run a `npm install` to add extra dependencies. If you don't need that, you can add `--skipInstall`~~

__We don't do that anymore since v1.4.0. You have to run install yourself (because there are just so many different npm alternative out there, and it's impossible for us to cover them all)__

## Additional configuration with server-io-core

There are a whole lot more options you can set with the [server-io-core](https://git@github.com/NEWBRAN-LTD/server-io-core). Especially
when you need the server side code to test with your Javascript code. You could use the proxy options. Have fun.

## Version 2

V.2 is complete rewrite in `ESM`. Not going to work on older node.js (pre 14-lts) anymore.

---

ISC

[Joel Chu](https://joelchu.com)

[NEWBRAN LTD](https://newbran.co.uk) (c) 2022
