import test from 'ava'
import { join, resolve as resolveDir } from 'node:path'
import { spawn } from 'node:child_process'
import fsx from 'fs-extra'
import debugFn from 'debug'
import { getDirname } from '../src/helpers.mjs'
import { makeTmps, removeDirs } from './fixtures/create-folders.mjs'

const debug = debugFn('create-qunit:test:basic')
const baseDir = getDirname(import.meta.url)
// this stupid Promise rule fucks up the test
const pathToCli = resolveDir(join(baseDir, '..', 'cli.mjs'))
const max = 3

// prepare test
test.before(async t => {
  await makeTmps(baseDir, max, '..')
})

test.after(t => {
  const keep = process.env.KEEP
  if (!keep) {
    removeDirs(baseDir, max)
  }
})

test.serial('It should able to run and copy fixtures files to the dest', async (t) => {
  t.plan(1)
  return new Promise((resolve) => {
    const baseDir0 = resolveDir((join(baseDir, 'tmp0')))
    const es = spawn(
      'node',
      [pathToCli],
      { cwd: baseDir0 }
    )
    es.stdout.on('data', data => {
      debug('stdout', `${data}`)
    })
    es.stderr.on('data', data => {
      debug('stderr', `${data}`)
    })
    es.on('close', code => {
      const target = join(baseDir0, 'tests', 'qunit', 'run-qunit.mjs')
      t.truthy(fsx.existsSync(target))
      // end test
      resolve(true)
    })
  })
})
// this keep throwing the 'you have to init your project shit'
test.serial('It should able to accept the baseDir parameter', async (t) => {
  t.plan(2)
  return new Promise((resolve) => {
    const baseDir1 = resolveDir((join(baseDir, 'tmp1')))
    // this will also test the v1.1.0 feature to search for tests folder
    fsx.ensureDir(join(baseDir1, 'tests'))
    const es = spawn('node',
      [pathToCli],
      { cwd: baseDir1 }
    )
    es.stdout.on('data', data => {
      debug('stdout', `${data}`)
    })
    es.stderr.on('data', data => {
      debug('stderr', `${data}`)
    })
    es.on('close', code => {
      debug('close with code', code)
      t.truthy(fsx.existsSync(join(baseDir1, 'tests', 'qunit', 'run-qunit.mjs')))
      // @2.0.0-beta this test is not making sense anymore
      // also test the v1.1.0 insert extra ignore into the ava part in package.json
      const pkg = fsx.readJsonSync(join(baseDir1, 'package.json'))
      // because it was added one before
      const inserted = pkg.ava.files.filter(file => file === '!tests/qunit/*.*').length
      t.true(inserted > 0)
      // end test
      resolve(true)
    })
  })
})

test.serial('It should able to add additonal paths to the webroot', async (t) => {
  t.plan(1)
  return new Promise((resolve) => {
    const baseDir2 = resolveDir((join(baseDir, 'tmp2')))
    fsx.ensureDirSync(join(baseDir2, '__tests__'))
    const es = spawn('node',
      [
        pathToCli,
        './dist',
        './node_modules',
        '--baseDir',
        './__tests__'
      ],
      { cwd: baseDir2 }
    )
    es.stdout.on('data', data => {
      debug('stdout', `${data}`)
    })
    es.stderr.on('data', data => {
      debug('stderr', `${data}`)
    })
    es.on('close', code => {
      debug('close with code', code)
      t.truthy(fsx.existsSync(join(baseDir2, '__tests__', 'qunit', 'run-qunit.mjs')))
      // end test
      resolve(true)
    })
  })
})
