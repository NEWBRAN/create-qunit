// breaking this function out to test it one by one
import { join, resolve as resolveDir } from 'node:path'
import fsx from 'fs-extra'

const tmpDirName = 'tmp'
const pkg = 'package.json'

/* main method */
export const makeTmpDir = async (baseDir, i, copy = '') => {
  return new Promise((resolve, reject) => {
    const newDir = tmpDirName + i
    fsx.ensureDir(join(baseDir, newDir), err => {
      if (err) {
        console.error(err)
        return reject(new Error(err))
      }
      if (copy !== '') {
        const targetPkgJson = resolveDir(baseDir, copy, pkg)

        return fsx.copy(
          targetPkgJson,
          join(baseDir, newDir, pkg)
        ).then(() => {
          resolve(true)
        })
      }
      resolve(true)
    })
  })
}

/* making numbers of temp directory */
export const makeTmps = async (baseDir, max, copy) => {
  const list = []
  for (let i = 0; i < max; ++i) {
    list.push(makeTmpDir(baseDir, i, copy))
  }
  return Promise.all(list)
}

/* clean up */
export const removeDirs = (baseDir, max) => {
  for (let i = 0; i < max; ++i) {
    fsx.removeSync(join(baseDir, tmpDirName + i))
  }
}
