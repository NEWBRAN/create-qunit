import test from 'ava'
import fsx from 'fs-extra'
import { getDirname } from '../src/helpers.mjs'
import { join } from 'node:path'

import { makeTmpDir, removeDirs } from './fixtures/create-folders.mjs'

const __dirname = getDirname(import.meta.url)
const baseDir = join(__dirname, 'fixtures')

test.after(() => {
  removeDirs(baseDir, 1)
})

test('should able to create a list of folders', async t => {
  await makeTmpDir(baseDir, 0)

  t.true(fsx.existsSync(join(baseDir, 'tmp0')))
})
