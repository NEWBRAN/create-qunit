// little helpers
import { join, dirname } from 'node:path'
import { fileURLToPath } from 'node:url'

export function getDirname (url) {
  const __filename = fileURLToPath(url)
  return dirname(__filename)
}

export function chainPromises (promises) {
  return promises.reduce((promiseChain, currentTask) => (
    promiseChain.then(chainResults => (
      currentTask.then(currentResult => (
        [...chainResults, currentResult]
      ))
    ))
  ), Promise.resolve([]))
}

export const baseConfig = {
  port: 0,
  webroot: [
    join('qunit', 'webroot'),
    join('qunit', 'files')
  ],
  open: true,
  reload: true,
  libFilePattern: join('lib', '*.lib.js'),
  testFilePattern: '*.qunit.js'
}
